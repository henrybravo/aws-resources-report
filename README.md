# aws-resources-report

Run a report on deployed AWS resources in a region, Lambda and EventBridge building blocks deployed using the AWS Serverless Application Model

![](AWS_resources_report.png)

## Prerequisites

- Install and configure AWS CLI
    - Installing or updating the latest version of the AWS CLI -> [getting-started-install](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
    - Configuring the AWS CLI -> [aws configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-config)
- Install SAM CLI -> [sam](https://aws.amazon.com/serverless/sam/)
- Enable AWS Config
    - [Setting Up AWS Config with the Console](https://docs.aws.amazon.com/config/latest/developerguide/gs-console.html)
    - [AWS Config best practices](https://aws.amazon.com/blogs/mt/aws-config-best-practices/)
- Ensure python 3.8 is installed as the Lambda will run on ARM64 architecture powered by Amazon Graviton processors

## Deploying the solution

1. After cloning this repository, you need to change the email address that you would like to use to send the reports to and optionally you can specify the aws accountid. Modify the `aws-resources-report/app.py` file lines 19 and 25 and specify the correct email address(es).

2. Then run:

`sam build`

`sam validate`

`sam deploy --guided`

3. Then we need to switch off the email validation requirement for AWS SNS by modifying the `aws-resources-report/app.py` file line 18 and set the value to 0. Asuming you did get an email with the subject: **"Amazon Web Services - Email Address Verification Request in region Europe (Ireland)"** and have clicked the link to verify and have been redirected to an AWS webpage with the header **"Congratulations! - you have successfully verified an email address"**

4. Then we build the package again propagating the change

`sam build`

`sam deploy --guided`

4. The solution is now ready and it will execute at 6pm UTC and send out a report to the email address
